# ASAP.V2 and ASAP.V3

Algorithm Selector and Prescheduler (ASAP).

Submitted to the [Open Algorithm Selection Challenge](http://www.coseal.net/open-algorithm-selection-challenge-2017-oasc/) (OASC).

ASAP.V1 and ASAP.V2 are fully described in:
[Gonard, F., Schoenauer, M., & Sebag, M. (2016, October). Algorithm Selector and Prescheduler in the ICON challenge. In International Conference on Metaheuristics and Nature Inspired Computing (META’2016).](https://hal.inria.fr/hal-01378745/document)

ASAP.V3 implements in addition a brute-force approach for determining the size of the pre-schedule.


## Installation

The example requires Python 3.5 or later and the following libraries:

* Numpy (numpy)
* SciPy (scipy)
* Pandas (pandas)
* Scikit-learn (sklearn)
* PyYAML (yaml)
* ARFF (liac-arff)
* CMA-ES (cma)
* ASlibScenario (https://github.com/mlindauer/ASlibScenario)

To install all dependencies locally please call:

```pip install -r requirements.txt --user```

(Most requirements are already satisfied by using Anaconda.)


## Setting up

All data files must be put into the ```data``` directory. To get the challenge data, navigate to the
repository main directory and type:

```
cd data/
wget http://www.coseal.net/wp-content/uploads/2017/06/oasc_scenarios.tar.gz
tar zxvf oasc_scenarios.tar.gz
cd ..
```


## Example Usage

Navigate to the ```src``` directory:

```cd src```

The example script ```run_asap.py``` produces ALL submissions of ASAP.V2 with:

```python run_asap.py```

Alternatively, run the following command to produce ASAP.V3 submissions:

```python run_asap.py v3```

Both look into the ```data/oasc_scenarios/train``` folder to get training data and into the
```data/oasc_scenarios/test``` to get test data. All folders listed in ```data/oasc_scenarios/train```
will be considered as scenarios. Change the ```datasets_to_run``` variable to ```True``` if you want
to run on a subset of the OASC scenarios.

A log file is written in, e.g. ```output/asap_v2_oasc/``` for ASAP.V2 and the required output are 
in, e.g. ```output/asap_v2_oasc/reg_weight_5e-03/```.

Optionally activate the pickling of the trained models with variable ```pickle_trained_models``` 
(beware of the disk space usage!). Otherwise only the JSON files are produced, e.g.: ```output/asap_v2_oasc/reg_weight_5e-03/Bado-test.json```


## License

ASAP.V2 is distributed under the GNU GPL version 3 licence.


## Contact

François Gonard
francois.gonard@irt-systemx.fr
