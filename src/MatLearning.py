################################################################################
# Réduction de modèles et Optimisation Multi-physiques
# Copyright (c) 2014, 2015, 2017 Institut de Recherche Technologique SystemX
# All rights reserved.
################################################################################
#    
#    This file is part of ASAP.V2.
#    
#    ASAP.V2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    ASAP.V2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with ASAP.V2.  If not, see <http://www.gnu.org/licenses/>.
#    


import os
import sys
import numpy as np
from scipy.stats import pearsonr
from sklearn.svm import SVR,SVC
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier, KNeighborsRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN,KMeans,AffinityPropagation

__author__ = "Francois Gonard"
__email__ = "francois.gonard@irt-systemx.fr"
__version__ = "2.0"
__license__ = "GPL version 3"


class MatLearning:
    
    def __init__(self, Xsample_, Ylearn_, learner_='SVR', learner_opts_=[]):
        ''' 
            Learn the mapping between features describing the instances and the performances of 
            algorithms on them
        
        Keyword arguments:
        Xsample_ -- array (number of instances,number of features)
        Ylearn_ -- array (number of instances,number of algorithms) or (number of instances,)
        '''
        self.sizeLearningSet, self.dimInputSpace = Xsample_.shape
        
        # First replace feature values equal to NaNs by the mean over non-NaN values of this feature
        self.mean_feature_values = np.nanmean(Xsample_, axis=0)
        self.missing_feature_mat = None
        self.missing_feature_proportion = None
        self.nonconstant_features = None
        Xsample_ = self.preprocess_features(Xsample_)
        
        # Fit the feature scaler on the training set and scale this set
        self.scaler = StandardScaler()
        self.scaler.fit(Xsample_)
        self.Xsample = self.scaler.transform(Xsample_)
        
        self.Ylearn = Ylearn_
        self.sizeLearningSet, self.dimInputSpace = self.Xsample.shape
        
        if Ylearn_.ndim == 2:
            _sizeLearningSet, self.dimOutputSpace = self.Ylearn.shape
        else:
            _sizeLearningSet, = self.Ylearn.shape
            self.dimOutputSpace = 1
        
        if (self.sizeLearningSet != _sizeLearningSet):
            print("Input data are corrupted")
            exit()
        self.learner = learner_
        try:
            self.learner_opts        = dict(learner_opts_)
        except:
            print("MatLearning could not interprete the learner options. Default values will be used")
        self._learntModels = []
        
        _learnModel = {'SVC'                    : self._learnModel_SVC,
                       'SVR'                    : self._learnModel_SVR,
                       'RandomForestClassifier' : self._learnModel_RandomForestClassifier,
                       'RandomForestRegressor'  : self._learnModel_RandomForestRegressor,
                       'KNeighborsClassifier'   : self._learnModel_KNeighborsClassifier,
                       'KNeighborsRegressor'    : self._learnModel_KNeighborsRegressor
                       }
        
        self._parse_learner_options()
        
        if np.isnan(self.Ylearn).any():
            print ("Missing values for known instances/algorithms are not yet supported")
            exit()
        
        models = _learnModel[self.learner](self.Xsample, self.Ylearn)
        for m in models:
            self._learntModels.append(m)
    
    
    def preprocess_features(self, feat_mat):
        '''
        Replace feature values equal to NaNs by the mean over non-NaN values of this feature. Then,
        delete feature whose values are constant over the training set.
        '''
        if feat_mat.ndim==2:
            nl,_ = feat_mat.shape
        else:
            nl = 1
            feat_mat = feat_mat.reshape((1,-1))
        mean_values_replicated = np.repeat(self.mean_feature_values.reshape((1,-1)), nl, axis=0)
        masked_feat_mat = np.ma.masked_array(feat_mat, np.isnan(feat_mat))
        
        if (self.missing_feature_mat is None) or (self.missing_feature_proportion is None):
            self.missing_feature_mat = masked_feat_mat.mask
            self.missing_feature_proportion = np.mean(self.missing_feature_mat,axis=0)
        
        new_feat_mat = masked_feat_mat.filled(mean_values_replicated)
        # Delete constant features
        if self.nonconstant_features is None:
            self.nonconstant_features = ~(np.var(new_feat_mat,axis=0)<1.e-6)
        new_feat_mat = new_feat_mat[:,self.nonconstant_features]
        
        return new_feat_mat
    
    
    def _parse_learner_options(self):
        
        self._allowed_options = {
            'SVR'                    : ['C','kernel','gamma','degree','epsilon','random_state'],
            'SVC'                    : ['C','kernel','gamma','random_state'],
            'RandomForestClassifier' : ['n_estimators','random_state'],
            'RandomForestRegressor'  : ['n_estimators','min_weight_fraction_leaf','random_state'],
            'KNeighborsClassifier'   : ['n_neighbors','weights','metric'],
            'KNeighborsRegressor'    : ['n_neighbors','weights','metric'],
            }
        
        self._valid_options = {}
        for opt,value in self.learner_opts.items():
            if opt in self._allowed_options[self.learner]:
                self._valid_options[opt] = value
            else:
                print("Option {0} is not supported with learner {1} and was ignored.".format(
                                                                                opt, self.learner))
    
    def _learnModel_SVC(self, Xsample, Ylearn):
        models = []
        svc = SVC()
        svc.set_params(**self._valid_options)
        svc.fit(Xsample, Ylearn)
        models.append(svc)
        return(models)
    
    def _learnModel_SVR(self, Xsample, Ylearn):
        models = []
        self.feature_weight = np.zeros((self.dimOutputSpace, self.dimInputSpace))
        for k in range(self.dimOutputSpace):
            svr = SVR()
            svr.set_params(**self._valid_options)
            y = np.ravel(Ylearn[:,k])
            svr.fit(Xsample, y)
            models.append(svr)
        return models
    
    def _learnModel_RandomForestClassifier(self, Xsample, Ylearn):
        models = []
        rnForest = RandomForestClassifier()
        rnForest.set_params(**self._valid_options)
        rnForest.fit(Xsample, Ylearn)
        models.append(rnForest)
        return models
    
    def _learnModel_RandomForestRegressor(self, Xsample, Ylearn):
        models = []
        for k in range(self.dimOutputSpace):
            rnForest = RandomForestRegressor()
            rnForest.set_params(**self._valid_options)
            y = np.ravel(Ylearn[:,k])
            rnForest.fit(Xsample, y)
            models.append(rnForest)
        return models
    
    def _learnModel_KNeighborsClassifier(self, Xsample, Ylearn):
        models = []
        knn = KNeighborsClassifier()
        knn.set_params(**self._valid_options)
        knn.fit(Xsample, Ylearn)
        models.append(knn)
        return models
    
    def _learnModel_KNeighborsRegressor(self, Xsample, Ylearn):
        models = []
        for k in range(self.dimOutputSpace):
            knn = KNeighborsRegressor()
            knn.set_params(**self._valid_options)
            y = np.ravel(Ylearn[:,k])
            knn.fit(Xsample, y)
            models.append(knn)
        return models
    
    
    def predict(self, Xnew):
        
        # If several models have been learnt, find the most appropriate to be used provided Xnew's
        # missing features
        prediction_models = None
        try:
            prediction_models = self._learntReducedModels[self.find_appropriate_cluster(np.isnan(Xnew).astype(int))]
        except AttributeError:
            prediction_models = self._learntModels
        
        # Scale the Xnew
        Xnew = self.preprocess_features(Xnew)
        Xnew = self.scaler.transform(Xnew)
        
        y_pred = np.zeros(self.dimOutputSpace)
        for k in range(len(prediction_models)):
            y_new = np.array(prediction_models[k].predict(Xnew)).astype(float)
            y_pred[k:k+len(y_new)] = y_new
        
        return y_pred.tolist()
    
    
    def __repr__(self):
        s = "Dimension of input space: {0}".format(self.dimInputSpace) + os.linesep
        s += "Dimension of output space: {0}".format(self.Ylearn.shape[1]) + os.linesep
        s += "Size of the training set: {0}".format(self.sizeLearningSet) + os.linesep
        for model in self._learntModels:
            s += str(model) + os.linesep
        
        return s

