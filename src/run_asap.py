import os
import sys
import math
import numpy as np
import pandas as pd
import scipy.stats as ss
import matplotlib.pyplot as plt
import sklearn
from itertools import combinations
import datetime
import pickle
import socket
from contextlib import redirect_stdout
from itertools import product, combinations, combinations_with_replacement

import asap_v2


learner = 'RandomForestRegressor'
opts = [('n_estimators',200)]

##### Options for running #####

#mode = 'val' # cross-validation on the OASC training data
mode = 'test' # train on the OASC training data and output submission as JSON files

# where is the data and output directories
general_output_dir = os.path.join('..','output')
datadir = os.path.join('..','data','oasc_scenarios','train')
testdir = os.path.join('..','data','oasc_scenarios','test')
datasets_to_run = sorted(os.listdir(datadir))

# true output directory is in ''general_output_dir''/''xp_name''
xp_name = 'asap_v2_oasc'
# if ASAP.V3 is asked in command line, it wille be changed to:
# xp_name = 'asap_v3_oasc'

seed = 1234

# To run in parallel, activate max_process > 1
max_process = 15    ### critical, beware of the size of the memory used!
if False:           # put your own safety condition there
    pass
else:
    max_process = 1     ### for safety for now ###

pickle_trained_models = False
# Uncomment if you want to pickle the systems. Beware, it is 2.6G for the OASC submission (and
# much more in the 10 fold cross-validation "val" mode).
# pickle_trained = True

verbosity_level = 2

##### / Options #####

i_split_list = np.arange(10)
if mode == 'test':
    i_split_list = [0]
list_cfg = ['reg_weight_{:.0e}'.format(w) for w in [0.005]]


def configure(system, cfg=''):
    
    system.learner_ = learner
    system.learner_opts = dict(opts)
    
    system.add_ft_runstatus = True
    system.add_ft_preschedule = True
    system.selection_weight_threshold = 1.e-5
    system.size_preschedule = 3
    
    if 'reg_weight' in cfg:
        system.regularization_weight = float(cfg.split('_')[-1])
    if 'relaxps' in cfg:
        system.relax_size_preschedule = True
        system.size_preschedule = 4


def execute_split(cfg, dataset, i_split):
    
    outputdir_cfg = os.path.join(outputdir, cfg)
    if os.path.exists(os.path.join(outputdir_cfg,'{}-{}.pickle'.format(dataset,i_split))):
        with open(os.path.join(outputdir_cfg,'{}-{}.pickle'.format(dataset,i_split)), 'br') as fin:
            assystem = pickle.load(fin)
    else:
        try:
            if not(os.path.exists(os.path.join(outputdir_cfg))):
                os.mkdir(os.path.join(outputdir_cfg))
        except:
            print('An error happened when creating the output folder.', flush=True)
            if not(os.path.exists(os.path.join(outputdir_cfg))):
                os.mkdir(os.path.join(outputdir_cfg))

        file_feature_values     = os.path.join(datadir, dataset, 'feature_values.arff')
        file_feature_runstatus  = os.path.join(datadir, dataset, 'feature_runstatus.arff')
        file_algorithm_runs     = os.path.join(datadir, dataset, 'algorithm_runs.arff')
        file_cross_validation   = None

        assystem = \
             asap_v2.ASAP_V2(os.path.join(datadir, dataset),
                             os.path.join(testdir, dataset),
                             learner_ = learner,
                             learner_opts_ = opts,
                             performance_measure_ = 'PAR10',
                             performance_learnt_ = 'PAR10',
                             print_to_file_ = False,
                             name_file_out_ = os.path.join(outputdir_cfg, 'result.{}.{}.out'.format(dataset,i_split)),
                             size_preschedule_ = 0,
                             max_runtime_preschedule_ = -1,
                             random_seed = seed,
                             verbosity=0
                             )
        
        if verbosity_level>=1:
            print(assystem.name_file_out)
        
        assystem.dataset_name = dataset
        assystem.cfg = cfg

        ####################
        configure(assystem, cfg)
        ####################
        
        if mode=='val':
            if not(assystem._import_cross_validation(assystem.cross_validation_filepath)):
                np.random.seed(seed)
                idx = np.arange(assystem.numInstance)
                np.random.shuffle(idx)
                idx_train = []
                idx_test = []
                for _ in i_split_list:
                    idx_test.append(idx[:len(idx)//len(i_split_list)])
                    idx_train.append(idx[len(idx)//len(i_split_list):])
                assystem.group_ids_test  = idx_test
                assystem.group_ids_train = idx_train
                assystem.xval = len(idx_test)
        else:
            assystem.group_ids_test  = [np.zeros((0,), dtype=int)]
            assystem.group_ids_train = [np.arange(assystem.numInstance)]
            assystem.xval = 1
        
        
        assystem._prepare_data()
        assystem.load_fold(i_split)
        
        assystem.train()
        
    if mode=='val':
        assystem.predict_on_test(os.path.join(outputdir_cfg,'{}-{}.csv'.format(dataset,i_split)))
    else:
        assystem.load_oasc_test()
        assystem.predict_on_test(os.path.join(outputdir_cfg,'{}-test.json'.format(dataset,i_split)),
                                 predict_oasc=True)
    
    if pickle_trained_models:
        with open(os.path.join(outputdir_cfg,'{}-{}.pickle'.format(dataset,i_split)), 'bw') as fout:
            pickle.dump(assystem, fout)


def process_split(args):
    
    cfg, dataset, i_split = args
    with open(os.path.join(outputdir, 'log.{}.out'.format(cfg)), 'a') as flog:
        with redirect_stdout(flog):
            print("PID: {} -- {}.{}".format(os.getpid(),dataset,i_split), flush=True)
    
    execute_split(cfg, dataset, i_split)


def execute_main():
    sys.setrecursionlimit(10000)    # for pickling
    
    args_list = []
    for cfg in list_cfg:
        for ds in datasets_to_run:
            for i_split in i_split_list:
                args_list.append((cfg,ds,i_split))
    
    pmax = int(max_process)
    if pmax > 1:
        from multiprocessing import Pool
        print('Using {} processes'.format(pmax), flush=True)
        with Pool(pmax) as p:
             p.map(process_split, args_list, chunksize=1)
    else:
        print('Using 1 process (for loop)'.format(pmax), flush=True)
        for args in args_list:
            process_split(args)


if __name__ == '__main__':
    if len(sys.argv)>1 and sys.argv[1]=='v3':
        xp_name = 'asap_v3_oasc'
        list_cfg = ['reg_weight_{:.0e}'.format(w) for w in [0.005]]
        list_cfg = ['relaxps_' + cfg for cfg in list_cfg]
    else:
        pass
    
    outputdir = os.path.join(general_output_dir, xp_name)
    if not(os.path.exists(outputdir)):
        os.mkdir(outputdir)
    
    execute_main()
