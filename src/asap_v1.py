################################################################################
# Réduction de modèles et Optimisation Multi-physiques
# Copyright (c) 2014, 2015, 2017 Institut de Recherche Technologique SystemX
# All rights reserved.
################################################################################
#    
#    This file is part of ASAP.V2.
#    
#    ASAP.V2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    ASAP.V2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with ASAP.V2.  If not, see <http://www.gnu.org/licenses/>.
#    

import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as ss
import scipy.linalg
import pickle
import sklearn

from sklearn.ensemble import RandomForestRegressor

import MatLearning
import ASlib_api

__author__ = "Francois Gonard"
__email__ = "francois.gonard@irt-systemx.fr"
__version__ = "2.0"
__license__ = "GPL version 3"


class ASAP_V1(ASlib_api.ASlib_api):
    """
    Algorithm selector and scheduler for the ICON challenge.
    """
    
    def __init__(self, 
                 train_dir,
                 test_dir=None,
                 learner_ = 'SVR',
                 learner_opts_ = [],
                 performance_measure_ = 'PAR10',
                 performance_learnt_ = 'PAR10',
                 print_to_file_ = False,
                 name_file_out_ = 'file.out',
                 size_preschedule_ = 3,
                 max_runtime_preschedule_ = -1,
                 random_seed = -1,
                 verbosity=0,
                 **kwargs
                 ):
        '''
        Initialize the object and imports the data.
        
        Parameters :
        ------------
        feature_values_filepath_ : string
            path to the file containing the features values
        filepath_runstatus : string
            path to the file containing the status of features computation
        filepath_performances : string
            path to the file containing information about the algorithm runs
        The options are self-explanatory
        '''
        super(ASAP_V1,self).__init__(train_dir,
                                     test_dir = test_dir,
                                     performance_measure_ = performance_measure_,
                                     performance_learnt_ = performance_learnt_,
                                     kwargs=kwargs
                                     )
        
        self.verbosity = verbosity
        
        self.learner                    = learner_
        self.learner_opts               = dict(learner_opts_)
        self.print_to_file              = print_to_file_
        self.name_file_out              = name_file_out_
        self.selection_weight_threshold = 1.e-3
        if random_seed<0:
            self.rand_st = np.random.RandomState(None)
        else:
            self.rand_st = np.random.RandomState(random_seed)
            self.random_seed = random_seed
        
        self.size_preschedule = int(size_preschedule_)
        if self.scenario.performance_type==['solution_quality']:
            # no preschedule
            self.size_preschedule = 0
            self.max_runtime_preschedule = 1
        if max_runtime_preschedule_ < 0:
            self.max_runtime_preschedule = 0.1*self.timeout
        elif max_runtime_preschedule_ < 1:
            self.max_runtime_preschedule = max_runtime_preschedule_*self.timeout
        else:
            self.max_runtime_preschedule = max_runtime_preschedule_
        
        self.add_ft_runstatus   = True
        self.add_ft_preschedule = True
    
    
    def train_feature_selection(self, feat_train, perf_train, threshold=1.e-5):
        '''
        Train a RandomForestRegressor on the provided data and select the features whose importance
        in the trained model is above the specified threshold. A boolean array of shape (numFeatures,)
        that specifies if each feature was selected is stored as a class attribute.
        
        Parameters :
        ------------
        feat_train : array (numInstances, numFeatures), dtype=float
            A feature matrix.
        perf_train : array (numInstances, numSolvers), dtype=float
            A performance matrix.
        threshold : float
            The threshold use to select features.
        
        Returns :
        ---------
        An array (numInstances, numSelectedFeatures), dtype=float
            The "selected feature version" of feat_train
        '''
        old_num_ft = feat_train.shape[1]
        feat_mat = feat_train
        perf = perf_train
        
        if feat_mat.ndim==2:
            nl,_ = feat_mat.shape
        
        # Impute the missing values with means along the feature
        mean_feature_values = np.nanmean(feat_mat, axis=0)
        mean_values_replicated = np.repeat(mean_feature_values.reshape((1,-1)), nl, axis=0)
        masked_feat_mat = np.ma.masked_array(feat_mat, np.isnan(feat_mat))
        new_feat_mat = masked_feat_mat.filled(mean_values_replicated)
        
        rf = RandomForestRegressor(n_estimators=10, random_state=self.rand_st)
        rf.fit(new_feat_mat,perf)
        self.is_a_selected_feature = rf.feature_importances_>threshold
        feat_train = feat_train[:,self.is_a_selected_feature]
        
        if self.verbosity:
            print('Feature selection: {0} --> {1}'.format(old_num_ft, feat_train.shape[1]))
        
        return feat_train
    
    
    def _identify_algorithms_for_preschedule(self, performance_mat, size_first_part_schedule=1,
                                             max_time_schedule=100):
        '''
        '''
        numInstances, numSolvers = performance_mat.shape
        if size_first_part_schedule>numSolvers:
            size_first_part_schedule = numSolvers
        
        from itertools import product
        
        step_size = max(5, max_time_schedule/100)
        timesteps = np.arange(0, max_time_schedule, step_size)
        
        if hasattr(self, 'relax_size_preschedule') and self.relax_size_preschedule==True:
            score_config = [{'algos':np.zeros((len(timesteps),icfg+1),dtype=int),
                           'rate_solved_instances': np.zeros(len(timesteps))}
                           for icfg in range(size_first_part_schedule)]
            
            for its,ts in enumerate(timesteps):
                for icfg in range(size_first_part_schedule):
                    
                    # All solving time < ts are set to 0
                    perf = np.where(performance_mat>ts/(icfg+1),performance_mat,0)
                    
                    # Search the best combination of algos
                    num_unsolved_instances = np.zeros((numSolvers,)*(icfg+1))
                    i_algos = (range(numSolvers),)*(icfg+1)
                    for algos in product(*i_algos):
                        perf_k_algos = np.hstack([perf[:,a].reshape((-1,1)) for a in algos])
                        num_unsolved_instances[algos] = np.count_nonzero( np.min( perf_k_algos ,axis=1) )
                        
                    score_config[icfg]['rate_solved_instances'][its] = \
                        (numInstances-np.min(num_unsolved_instances))/numInstances
                    score_config[icfg]['algos'][its,:] = np.unravel_index(np.argmin(num_unsolved_instances),
                                                                   (numSolvers,)*(icfg+1))

            i_best_config = np.argmax(np.stack([score_config[icfg]['rate_solved_instances']
                                              for icfg in range(size_first_part_schedule)],
                                             axis=1),
                                    axis=1)
            best_rate = np.hstack([score_config[i_best_config[its]]['rate_solved_instances'][its]
                                   for its,ts in enumerate(timesteps)])
            best_combination = [score_config[i_best_config[its]]['algos'][its]
                                for its,ts in enumerate(timesteps)]
            
            step_lim = self._identify_end_preschedule(timesteps, best_rate, max_time_schedule)
            
            ialgos_preschedule  = best_combination[step_lim]
            runtime_preschedule = np.asarray((timesteps[step_lim]/ialgos_preschedule.shape[0],)
                                             * ialgos_preschedule.shape[0])
            
            # Add new features: the fact that each solver of ialgos_preschedule has not solved the 
            # instance within runtime_preschedule
            new_features = np.where(performance_mat[:,ialgos_preschedule]
                                        > timesteps[step_lim]/ialgos_preschedule.shape[0],
                                    1.,0.)
            
            return (runtime_preschedule,
                    ialgos_preschedule,
                    new_features)
        else:
            best_config = {'algos':np.zeros((len(timesteps),size_first_part_schedule),dtype=int),
                           'rate_solved_instances': np.zeros(len(timesteps))}
            
            for its,ts in enumerate(timesteps):
                # All solving time < ts are set to 0
                perf = np.where(performance_mat>ts,performance_mat,0)
                
                # Search the best combination of algos
                num_unsolved_instances = np.zeros((numSolvers,)*size_first_part_schedule)
                i_algos = (range(numSolvers),)*size_first_part_schedule
                for algos in product(*i_algos):
                    perf_k_algos = np.hstack([perf[:,a].reshape((-1,1)) for a in algos])
                    num_unsolved_instances[algos] = np.count_nonzero( np.min( perf_k_algos ,axis=1) )
                
                best_config['rate_solved_instances'][its] = (numInstances-np.min(num_unsolved_instances))/numInstances
                best_config['algos'][its,:] = np.unravel_index(np.argmin(num_unsolved_instances),
                                                               (numSolvers,)*size_first_part_schedule)
            
            step_lim = self._identify_end_preschedule(timesteps, best_config['rate_solved_instances'],
                                                      max_time_schedule)
            
            # Add new features: the fact that each solver of best_config['algo'] has not solved the 
            # instance within timesteps[step_lim]
            new_features = np.where(performance_mat[:,best_config['algos'][step_lim,:]]>timesteps[step_lim],1.,0.)
            
            return (np.array((timesteps[step_lim],)*size_first_part_schedule),
                    best_config['algos'][step_lim,:],
                    new_features)
    
    
    def _identify_end_preschedule(self, timesteps, rate, max_time_schedule):
        '''
        '''
        normalized_rate = rate / (rate[-1]-rate[0])
        criterion = ((normalized_rate-normalized_rate[0])*(timesteps-0)
                     +(normalized_rate[-1]-normalized_rate)*(timesteps[-1]-timesteps))
        
        if timesteps[np.argmin(criterion)]<max_time_schedule/self.size_preschedule:
            return np.argmin(criterion)
        else:
            return len(timesteps)-1
    
    
    def selectAlgorithm(self, feat_train, perf_train, feat_test):
        '''
        Predict the performances of the algorithms on the test set and select the algorithm whose
        predicted performances are the best.
        
        Parameters
        ----------
        feat_train : array, shape (n_samples_training_set, n_features)
            The samples of the training set described as features.
        perf_train : array, shape (n_samples_training_set, n_algorithms)
            The performances of the algorithms on the training set.
        feat_test : array, shape (n_samples_test_set, n_features)
            The samples of the test set described as features.
        opts : list
            Some options.
        
        Returns
        -------
        best_algorithm_pred : array, shape (n_sample_test_set,1)
            The index of the selected algorithm for each instance in the test set.
        '''
        
        # A performance model is learnt, from which the predicted best algorithm is retrieved
        PerfModel = MatLearning.MatLearning(feat_train_, perf_train, 
                                            learner_ = self.learner, 
                                            learner_opts_=self.learner_opts)
        self.trained_PerfModel = PerfModel
        perf_test_pred = []
        for i,inst in enumerate(feat_test_):
            perf_test_pred.append(PerfModel.predict(inst))
            if np.isnan(perf_test_pred[-1]):
                print('NaN prediction')
        
        # Selection phase: select the algorithm whose performances are the best
        perf_test_pred = np.array(perf_test_pred, dtype=float)
        return self._selectKBestAlgorithmsFromPerformance(perf_test_pred, k=1)
    
    
    def _selectKBestAlgorithmsFromPerformance(self, performance_matrix, k=1):
        '''
        Select the most suitable algorithm considering a performance matrix.
        
        Parameters
        ----------
        performance_matrix : an array (n_samples_training_set, n_algorithms)
            The (possibly predicted) performances on n_samples_training_set instances.
        k : int
            The number of best algorithms to return
        
        Returns
        -------
        An array (n_samples_training_set,k), dtype=int
            The index of the best algorithms per instance sorted (first element is the best)
        '''
        if (k<1):
            k = 1
        if (k>performance_matrix.shape[1]):
            k = performance_matrix.shape[1]
        
        if self.performance_learnt == 'rating':     # the higher is the better
            return np.argsort(-performance_matrix, axis=1)[:,:k]  # minus to sort decreasingly
        else:                                       # the lower is the better
            a1 = [[i for b in range(self.numAlg) if row[b]==np.amax(row)] for i,row in enumerate(performance_matrix)]
            counter = [len(best) for best in a1]
            return np.argsort(performance_matrix, axis=1)[:,:k]
    
    
    def _add_solvers_to_schedule(self, schedule, solvers, runtimes, pos='beg'):
        '''
        '''
        global_schedule = pd.DataFrame(np.hstack([schedule['instanceID'], schedule['runID'],
                                                  schedule['solver'], schedule['time_limit']]),
                                       columns=['inst','runid','algo','time']
                                       )
        global_schedule['runid'] = global_schedule['runid'].astype(int)
        global_schedule['time'] = global_schedule['time'].astype(float)
        instanceID_noduplicates = global_schedule['inst'].drop_duplicates().as_matrix().astype(str)
        
        new_schedule = {}
        new_schedule['instanceID'] = []
        new_schedule['solver'] = []
        new_schedule['runID'] = []
        new_schedule['time_limit'] = []
        for inst in instanceID_noduplicates:
            instance_schedule = global_schedule[global_schedule['inst']==inst].sort_values(
                                by='runid',inplace=False).as_matrix(columns=['inst','runid','algo','time'])
            
            if (pos=='beg'):
                cur_runid = int(0)
                cur_runtime = 0.
                for (s,t) in zip(solvers.ravel(),runtimes.ravel()):
                    if (s not in (instance_schedule[:,2].ravel())) and (cur_runtime+t<self.timeout):
                        cur_runid   += 1
                        cur_runtime += t
                        new_schedule['instanceID'].append(inst)
                        new_schedule['solver'].append(s)
                        new_schedule['runID'].append(cur_runid)
                        new_schedule['time_limit'].append(t)
                
                for sch in instance_schedule:
                    runid = int(sch[1])
                    solver = sch[2]
                    time = float(sch[3])
                    if (cur_runtime+time < self.timeout):
                        # add the step to the instance scehedule
                        new_schedule['instanceID'].append(inst)
                        new_schedule['solver'].append(solver)
                        new_schedule['runID'].append(runid + cur_runid)
                        new_schedule['time_limit'].append(time)
                        cur_runtime += time
                    else:
                        # this step will be shorter than initially and will be the last
                        new_schedule['instanceID'].append(inst)
                        new_schedule['solver'].append(solver)
                        new_schedule['runID'].append(runid + cur_runid)
                        new_schedule['time_limit'].append(self.timeout-cur_runtime)
                        break
        
        new_schedule['instanceID'] = np.array(new_schedule['instanceID'],dtype=str).reshape((-1,1))
        new_schedule['solver']     = np.array(new_schedule['solver'],dtype=str).reshape((-1,1))
        new_schedule['runID']      = np.array(new_schedule['runID'],dtype=int).reshape((-1,1))
        new_schedule['time_limit'] = np.array(new_schedule['time_limit'],dtype=float).reshape((-1,1))
        
#        self.dump_algorithm_selection('suggestion1.txt',
#                                          new_schedule['instanceID'],
#                                          new_schedule['runID'],
#                                          new_schedule['solver'],
#                                          new_schedule['time_limit'])
        
        return new_schedule
    
    
    def _get_time_to_solve_per_instance(self, performanceOut_test_set, ialgos_preschedule,
                                        runtimes_preschedule, best_algo_pred):
        '''
        Implements the skipping of solvers when they appear in both the preschedule and 
        best_algo_pred
        '''
        time_to_solve = np.zeros((performanceOut_test_set.shape[0],), dtype=float)
        is_solved_per_algo = performanceOut_test_set[:,ialgos_preschedule]<runtimes_preschedule
        is_already_solved = np.zeros((performanceOut_test_set.shape[0],), dtype=bool)
        
        suggested_algorithm_there = (ialgos_preschedule.reshape((-1,1)) == best_algo_pred)
        suggested_algorithm_there_any = np.any(suggested_algorithm_there,axis=0)
        no_in_preschedule = np.argmax(suggested_algorithm_there,axis=0)
        
        for runid in range(len(ialgos_preschedule)):
            is_newly_solved = is_solved_per_algo[:,runid] * (is_already_solved==False)
            runid_is_skipped = (suggested_algorithm_there_any==True) * (no_in_preschedule==runid)
            is_newly_solved *= (runid_is_skipped==False)
            time_to_solve[is_newly_solved] += performanceOut_test_set[:,ialgos_preschedule[runid]][is_newly_solved]
            is_already_solved += is_newly_solved
            time_to_solve[((is_already_solved==False)*(runid_is_skipped==False))==True] += runtimes_preschedule[runid]
            
        # instances not solved by the preschedule
        perf_selected_algorithm = np.array([perf[best_algo_pred[i]]
                                            for i,perf in enumerate(performanceOut_test_set)])
        # For the cases where the suggested algorithm is part of the preschedule and the preschedule
        # did not solved it, the suggested algorithm has already been running for a little time
        time_to_solve[is_already_solved==False] += perf_selected_algorithm[is_already_solved==False]
        time_to_solve[time_to_solve>self.timeout] = 10*self.timeout
        #is_already_solved[time_to_solve<self.timeout] = True
        
        return time_to_solve, is_already_solved
    
    
    def _get_time_to_solve(self, performanceOut, algorithm_description, step_solver_description, 
                           runtimes, time_limit=-1):
        '''
        Contrary to the official rules of the competition, solvers can appear twice (once in the 
        preschedule and once outside).
        '''
        if time_limit<0:
            time_limit = self.timeout
        
#        num_algo,num_algo_feat = algorithm_description.shape
        n_step = len(runtimes)
        
        if step_solver_description.ndim > 1:
            if np.max(step_solver_description) > np.max(algorithm_description)+1:
                return performanceOut.shape[0]*10*timeout
            if np.min(step_solver_description) < np.min(algorithm_description)-1:
                return performanceOut.shape[0]*10*timeout
        
        # Identify the algorithms
        if step_solver_description.ndim == 1:
            i_solvers = step_solver_description
        else:
            num_algo,num_algo_feat = algorithm_description.shape
            i_solvers = np.zeros((n_step,),dtype=int)
            for i_s,s in enumerate(step_solver_description):
                i_solvers[i_s] = np.argmin(np.linalg.norm(algorithm_description-s,axis=1))
        
        time_to_solve = np.zeros((performanceOut.shape[0],), dtype=float)
        is_solved_per_algo = performanceOut[:,i_solvers]<np.abs(runtimes)
        is_already_solved = np.zeros((performanceOut.shape[0],), dtype=bool)
        is_newly_solved = np.zeros((is_already_solved.shape), dtype=bool)
        for runid in range(n_step):
            is_newly_solved[:] = is_solved_per_algo[:,runid] * (is_already_solved==False)
            time_to_solve[is_newly_solved] += performanceOut[:,i_solvers[runid]][is_newly_solved]
            is_already_solved += is_newly_solved
            time_to_solve[(is_already_solved==False)] += np.abs(runtimes)[runid]
        
        time_to_solve[time_to_solve>self.timeout] = 10.*self.timeout
        time_to_solve[is_already_solved==False] = 10.*self.timeout
        
        return time_to_solve
    
    
    def load_fold(self, i_fold):
        '''Load all data in class variables of the form:
        self.<dataname>_train
        self.<dataname>_test
        '''
        self.rand_st = np.random.RandomState(self.random_seed)
        
        idx_train = self.group_ids_train[i_fold]
        idx_test  = self.group_ids_test[i_fold]
        
        # Create training and test sets
        self.instanceList_train = self.instance_list[idx_train]
        self.instanceList_test  = self.instance_list[idx_test]
        
        self.performanceoriginal_train = self.original_performance_matrix[idx_train]
        self.performanceoriginal_test  = self.original_performance_matrix[idx_test]
        
        self.feature_train = self.feature_matrix[idx_train]
        self.feature_test  = self.feature_matrix[idx_test]
        
        self.performanceOut_train = self.performanceOut_matrix[idx_train]
        self.performanceOut_test  = self.performanceOut_matrix[idx_test]
        
        self.performanceForLearning_train = self.performanceForLearning_matrix[idx_train]
        self.performanceForLearning_test  = self.performanceForLearning_matrix[idx_test]
        
        self.runstatusOK_train = self.runstatusOK_matrix[idx_train]
        self.runstatusOK_test  = self.runstatusOK_matrix[idx_test]
        
        self.ranking_train = self.ranking_matrix[idx_train]
        self.ranking_test  = self.ranking_matrix[idx_test]
        
        self.feature_runstatus_train = None
        self.feature_runstatus_test  = None
        try:
            self.feature_runstatus_train = self.feature_runstatus[idx_train]
            self.feature_runstatus_test  = self.feature_runstatus[idx_test]
        except AttributeError:
            pass
        
        self.feature_group_cost_train = self.feature_group_cost[idx_train]
        self.feature_group_cost_test  = self.feature_group_cost[idx_test]
    
    
    def load_oasc_test(self):
        ''''''
        assert self.test_scenario is not None
        
        self.instanceList_test  = np.asarray(self.test_scenario.feature_data.index.tolist())
        
        # from aslib_scenario, i assume all pandas.DataFrame have been sorted by the index column
        #
        # be careful importing the features in the same order as for the train set
        self.feature_test           = self.test_scenario.feature_data[self.feature_names
                                        ].as_matrix().astype(np.float32)
        self.feature_runstatus_test = self.test_scenario.feature_runstatus_data[self.feature_group_names
                                        ].as_matrix().astype(str)
        
        import warnings
        warnings.warn('only "provided" keyword is handled')
        warnings.warn('what if feature cost if not provided?')
    
    
    def feature_runstatus_to_numeric(self, feature_group_rs):
        '''
        '''
        # Turns the feature groups runstatus matrix into a boolean-like matrix
        feature_group_rs_asnumeric = np.repeat(np.zeros(feature_group_rs.shape, dtype=float),
                                               len(self.list_of_runstatus), axis=1)
        n_fg = len(self.feature_group_names)
        for ind,rs in enumerate(self.list_of_runstatus):
            feature_group_rs_asnumeric[:,ind*n_fg:(ind+1)*n_fg] = (feature_group_rs==rs)
        
        return feature_group_rs_asnumeric
    
    
    def _prepare_data(self):
        '''
        Remove instances that are not solved by any algorithm and initialize the performance 
        matrices used for training and for the objective.
        '''
        if self.scenario.performance_type==['solution_quality']:
            # no preschedule
            self.size_preschedule = 0
            self.max_runtime_preschedule = 1
        self.total_num_singleBest_fails = 0
        
        # Update the number of instances
        self.numInstance, self.numAlg = self.original_performance_matrix.shape
        
        self.performanceOut_matrix = self.original_performance_matrix.copy()
        
        self.ranking_matrix = np.zeros(self.performanceOut_matrix.shape)
        for i_inst in range(self.numInstance):
            # Beware of the method used for ranking! It may lead to non-integer ranks!
            self.ranking_matrix[i_inst] = ss.rankdata(self.performanceOut_matrix[i_inst],
                                                      method='average')
        
        if self.performance_measure == 'runtime':
            pass
        elif self.performance_measure == 'PAR10':
            # PAR10: penalty for instances not solved (runtime = timeout*10)
            self.performanceOut_matrix[np.logical_not(self.runstatusOK_matrix)] = self.timeout*10
        else:
            print('{} is not supported as a measure of performance.'.format(self.performance_measure))
        
        self.performanceForLearning_matrix = self.original_performance_matrix.copy()
        if self.performance_learnt == 'runtime':
            pass
        elif self.performance_learnt == 'PAR10':
            # PAR10: penalty for instances not solved (runtime = timeout*10)
            self.performanceForLearning_matrix[np.logical_not(self.runstatusOK_matrix)] = self.timeout*10
        elif self.performance_learnt == 'rank':
            self.performanceForLearning_matrix = self.ranking_matrix
        elif self.performance_learnt == 'rating':
            # basically turns the ranking from 1 to numAlg into a rating from numAlg (the best) to 1
            self.performanceForLearning_matrix = -(self.ranking_matrix) + (self.numAlg+1)
        else:
            print('{} is not supported as a learning performance value.'.format(
                                                                        self.performance_measure))
        
        # The overall single best must be computed outside the cross-validation. It differs from the
        # single best at each fold since the latter are the best only on the training set and not on
        # the complete dataset.
        self.overall_single_best = np.argmin(np.sum(self.performanceOut_matrix,axis=0))
    
    
    def preprocess_features(self, ft_original, ft_runstatus, ft_preschedule):
        '''
        '''
        ft_preprocessed = ft_original.copy()
        if self.add_ft_runstatus and ft_runstatus is not None:
            # Turns the feature groups runstatus matrix into a boolean-like matrix
            ft_runstatus_asnumeric = self.feature_runstatus_to_numeric(ft_runstatus)
            # Add feature runstatus features
            ft_preprocessed = np.hstack([ft_preprocessed, ft_runstatus_asnumeric])
        if self.add_ft_preschedule and ft_preschedule is not None:
            # Add preschedule features
            ft_preprocessed = np.hstack([ft_preprocessed, ft_preschedule])
        
        return ft_preprocessed
    
    
    def train(self):
        '''
        '''
        self.size_preschedule = min(self.size_preschedule,self.numAlg-1)
        if self.size_preschedule>0:
            (self.runtimes_preschedule, 
             self.ialgos_preschedule,
             self.features_preschedule_train) = self._identify_algorithms_for_preschedule(
                                                  self.performanceOut_train,
                                                  size_first_part_schedule=self.size_preschedule,
                                                  max_time_schedule=self.max_runtime_preschedule
                                                  )
        else:
            self.runtimes_preschedule = np.zeros((0,))
            self.ialgos_preschedule = np.zeros((0,)).astype(int)
            self.features_preschedule_train = None
        
        # Additional features
        self.full_feature_train = self.preprocess_features(self.feature_train,
                                                           self.feature_runstatus_train,
                                                           self.features_preschedule_train)
        
        # Feature selection
        self.feature_train = self.train_feature_selection(self.full_feature_train,
                                                          self.performanceForLearning_train,
                                                          self.selection_weight_threshold)
        
        (self.instanceList_train,
         self.feature_train,
         self.performanceOut_train,
         self.performanceForLearning_train,
         self.ranking_train,
         self.runstatusOK_train,
         self.feature_runstatus_train
         ) = self._drop_unsolvable_instances(self.instanceList_train,
                                             self.feature_train,
                                             self.performanceOut_train,
                                             self.performanceForLearning_train,
                                             self.ranking_train,
                                             self.runstatusOK_train,
                                             self.feature_runstatus_train
                                             )
        
        if ('KNeighbors' not in self.learner):
            self.learner_opts['random_state'] = self.rand_st
        
        self.PerfModel = MatLearning.MatLearning(self.feature_train, self.performanceForLearning_train, 
                                                 learner_ = self.learner, 
                                                 learner_opts_=self.learner_opts)
    
    
    def predict_on_test(self, name_output_file, **kwargs):
        '''
        '''
        if 'predict_oasc' in kwargs and kwargs['predict_oasc']==True:
            return self.predict_oasc(self.instanceList_test, self.feature_test, self.feature_runstatus_test,
                            name_output_file, **kwargs)
        return self.predict(self.instanceList_test, self.feature_test, self.feature_runstatus_test,
                            name_output_file, **kwargs)
    
    
    def predict_on_input_file(self, new_feature_values_file, new_feature_runstatus_file,
                              name_output_file, **kwargs):
        '''
        Given path to a feature_values file and a feature_runstatus file, predict a schedule and 
        writes it to name_output_file in a csv format.
        '''
        
        (new_instance_list,
         new_feature_matrix,
         new_feature_runstatus) = self._importFeatures_liac(new_feature_values_file,
                                                            new_feature_runstatus_file)
        
#        assert(new_feature_matrix.shape[1]==self.feature_matrix.shape[1])
        
        return self.predict(new_instance_list, new_feature_matrix, new_feature_runstatus,
                            name_output_file, **kwargs)
    
    
    def predict(self, new_instance_list, new_feature_matrix, new_feature_runstatus,
                name_output_file, verbose=0, **kwargs):
        
        size_test = len(new_feature_matrix)
        
        size_preschedule = len(self.runtimes_preschedule)
#        if size_preschedule>0:
        ft_preschedule_simulated = np.ones((size_test,size_preschedule))
#        else:
#            ft_preschedule_simulated = None
        
        # Add new features
        new_feature_matrix = self.preprocess_features(new_feature_matrix,
                                                      new_feature_runstatus,
                                                      ft_preschedule_simulated)
        
        # Perform feature selection
        new_feature_matrix = new_feature_matrix[:,self.is_a_selected_feature]
        
        # Predict performances to select the best predicted solver from
        perf_pred = np.zeros((size_test,self.numAlg))
        for i,inst in enumerate(new_feature_matrix):
            perf_pred[i] = self.PerfModel.predict(inst)
        selected_solver = self._selectKBestAlgorithmsFromPerformance(perf_pred, k=1).reshape((-1,))
        
        # Build the output schedule
        runID      = np.ones(new_instance_list.shape,dtype=int).reshape((-1,1))
        time_limit = self.timeout*np.ones(new_instance_list.shape).reshape((-1,1))
        suggested_algorithm = self.algorithm_list[selected_solver].reshape((-1,1))
        schedule = {}
        schedule['instanceID'] = new_instance_list.reshape((-1,1))
        schedule['runID'] = runID
        schedule['solver'] = suggested_algorithm
        schedule['time_limit'] = time_limit
        
        preschedule_solvers  = np.array(self.algorithm_list,dtype=str)[self.ialgos_preschedule].reshape((-1,1))
        preschedule_runtimes = self.runtimes_preschedule.reshape((-1,1))
        schedule = self._add_solvers_to_schedule(schedule,
                                                 preschedule_solvers,
                                                 preschedule_runtimes,
                                                 pos='beg')
        
        try:
            par10, solved = self.compute_ICONchallenge_score(schedule['instanceID'],
                                                             schedule['runID'],
                                                             schedule['solver'],
                                                             schedule['time_limit'],
                                                             count_feature_cost=False)
            
            if self.verbosity:
                print('size_test: {}'.format(size_test))
                print('PAR10: {}'.format(par10))
                print('solved: {}'.format(solved), flush=True)
        except:
            pass
        
        self.dump_algorithm_selection(name_output_file,
                                      schedule['instanceID'],
                                      schedule['runID'],
                                      schedule['solver'],
                                      schedule['time_limit'])
    
    
    def predict_oasc(self, new_instance_list, new_feature_matrix, new_feature_runstatus,
                     name_output_file, verbose=0, **kwargs):
        '''
        Differences with the predict method:
        * feature steps are included in the output schedule
        * output schedule follows the JSON output required in the OASC challenge
        * there can be several times the same algorithm in the schedule (in particular, once in the
          (preschedule and once as the selected algorithm)
        
        Unless further changes, the output schedule is composed of:
        1) the preschedule steps (as many as there are algorithms in the preschedule)
        2) the feature computation (all features, in the same order as they are read in the data)
        3) the selected algorithm
        Exceptions: qualitative datasets: there is only the selected algorithm
        '''
        
        size_test = len(new_feature_matrix)
        
        size_preschedule = len(self.runtimes_preschedule)
        ft_preschedule_simulated = np.ones((size_test,size_preschedule))
        
        # Add new features
        new_feature_matrix = self.preprocess_features(new_feature_matrix,
                                                      new_feature_runstatus,
                                                      ft_preschedule_simulated)
        
        # Perform feature selection
        new_feature_matrix = new_feature_matrix[:,self.is_a_selected_feature]
        
        # Predict performances to select the best predicted solver from
        perf_pred = np.zeros((size_test,self.numAlg))
        for i,inst in enumerate(new_feature_matrix):
            perf_pred[i] = self.PerfModel.predict(inst)
        selected_solver = self._selectKBestAlgorithmsFromPerformance(perf_pred, k=1).reshape((-1,))
        
        
        def add_feature_steps(feature_group_names, **kwargs):
            '''Built the feature steps so that:
            * feature steps precedence hold
            * feature that presolve the most instances are computed first
            
            Caveat: i must assume that feature_group_names and the columns of 
                    self.feature_runstatus_train match exactly
            '''
            
            # order the feature groups so that those which presolve the maximum number of instances 
            # are first
            if "presolved" in self.feature_runstatus_train:
                feature_group_computation_order = np.asarray(feature_group_names)[
                    np.argsort(
                        np.sum(np.asarray(self.feature_runstatus_train)=="presolved", axis=0)
                        )[::-1]
                    ]
            else:
                feature_group_computation_order = np.asarray(feature_group_names)
            
            # utility function
            def add_precedence(ft, dict_precedence, current_list):
                # Add (recursively) feature required by ''ft'' and (then) ''ft' at the end of 
                # ''current_list''
                # ''current_list'' keeps all unique elements.
                # Return the updated ''current_list''
                for prec in dict_precedence[ft]:
                    new_list = add_precedence(prec, dict_precedence, current_list)
                    current_list = current_list + [new_ft for new_ft in new_list
                                                   if new_ft not in current_list]
                
                if ft in current_list:
                    return current_list
                else:
                    return current_list + [ft]
                
                return current_list
                
            feature_steps = []
            for ft_grp in feature_group_computation_order:
                if ft_grp not in feature_steps:
                    # check for precedence and add them just before ft_grp
                    feature_steps = add_precedence(ft_grp, self.feature_group_precedence, feature_steps)
            
            return feature_steps
        
        
        if self.scenario.performance_type[0] == "runtime":
            # Build the output schedule
            
            # same for all instances: preschedule algorithms
            # TODO: check is these feature groups are really all used (self.is_a_selected_feature)
            schedule_for_all = [(self.algorithm_list[int(alg)],float(rt))
                                for alg,rt in zip(self.ialgos_preschedule,self.runtimes_preschedule)]
            
            # same for all instances: features
            feature_group_selected = self.feature_group_names
            # there select only feature groups used
            feature_group_selected = add_feature_steps(feature_group_selected)
            schedule_for_all += feature_group_selected
        elif self.scenario.performance_type[0] == "solution_quality":
            schedule_for_all = []
        
        # Final schedule
        schedule = {
            inst : schedule_for_all + [(self.algorithm_list[int(selected)],float(self.timeout))]
            for inst,selected in zip(new_instance_list,selected_solver)
            }
        
        self.dump_schedule_oasc(schedule, name_output_file)
        
        return schedule
    
    
    def get_repr_str(self):
        '''
        '''
        str_to_file  = 'feature_values_filepath = \'{0}\''.format(self.feature_values_filepath) 
        str_to_file += os.linesep
        str_to_file += 'feature_runstatus_filepath = \'{0}\''.format(self.feature_runstatus_filepath) 
        str_to_file += os.linesep
        str_to_file += 'algorithm_runs_filepath = \'{0}\''.format(self.algorithm_runs_filepath)
        str_to_file += os.linesep
        str_to_file += 'performance_measure = {0}'.format(self.performance_measure) + os.linesep
        str_to_file += 'performance_learnt = {0}'.format(self.performance_learnt) + os.linesep
        str_to_file += 'numAlg = {}'.format(self.numAlg) + os.linesep
        str_to_file += 'numInstance = {}'.format(self.numInstance) + os.linesep
        str_to_file += 'numFeatureGroups = {}'.format(self.numFeatureGroups) + os.linesep
        str_to_file += 'timeout = {}'.format(self.timeout) + os.linesep
        str_to_file += 'max_runtime_preschedule = {}'.format(self.max_runtime_preschedule) + os.linesep
        str_to_file += 'size_preschedule = {}'.format(self.size_preschedule) + os.linesep
        str_to_file += 'selection_weight_threshold = {}'.format(self.selection_weight_threshold) + os.linesep
        str_to_file += 'add_ft_preschedule = {}'.format(self.add_ft_preschedule) + os.linesep
        str_to_file += 'add_ft_runstatus = {}'.format(self.add_ft_runstatus) + os.linesep
        str_to_file += 'learner = {0}'.format(self.learner) + os.linesep
        str_to_file += 'opt = {0}'.format(self.learner_opts) + os.linesep
        str_to_file += self.__class__.__name__ + os.linesep
        str_to_file += '-'*10 + os.linesep
        
        return str_to_file




