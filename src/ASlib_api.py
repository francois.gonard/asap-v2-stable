################################################################################
# Réduction de modèles et Optimisation Multi-physiques
# Copyright (c) 2014, 2015, 2017 Institut de Recherche Technologique SystemX
# All rights reserved.
################################################################################
#    
#    This file is part of ASAP.V2.
#    
#    ASAP.V2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    ASAP.V2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with ASAP.V2.  If not, see <http://www.gnu.org/licenses/>.
#    

import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as ss
import arff

#import aslib_scenario
from aslib_scenario import aslib_scenario

__author__ = "Francois Gonard"
__email__ = "francois.gonard@irt-systemx.fr"
__version__ = "2.0"
__license__ = "GPL version 3"


class ASlib_api(object):
    """
    Class to read data, write results into the shell or files and evaluate algorithm selectors and 
    schedulers on the ASlib data (tested with ASlib 1.0.1).
    """
    
    def __init__(self, 
             train_dir,
             test_dir=None,
             performance_measure_ = 'PAR10',
             performance_learnt_ = 'PAR10',
             cross_val = 1,
             **kwargs
             ):
        '''
        Initialize the object and imports the data.
        
        Parameters :
        ------------
        feature_values_filepath_ : string
            path to the file containing the features values
        filepath_runstatus : string
            path to the file containing the status of features computation
        filepath_performances : string
            path to the file containing information about the algorithm runs
        The options are self-explanatory
        '''
        self.feature_values_filepath    = os.path.join(train_dir,'feature_values.arff')
        self.feature_runstatus_filepath = os.path.join(train_dir,'feature_runstatus.arff')
        self.algorithm_runs_filepath    = os.path.join(train_dir,'algorithm_runs.arff')
        self.cross_validation_filepath  = None
        self.dataset_name               = None
        self.learnDirectFromFeatures    = True
        self.performance_measure        = performance_measure_
        self.performance_learnt         = performance_learnt_
        self.str_to_file                = ''
        self.use_challenge_split        = False
        if cross_val<1:
            self.xval = 1   # the number of folds of the cross-validation. Only relevant if no
                            # cross-validation file has been provided.
        else:
            self.xval = int(cross_val)
        
        self.scenario = aslib_scenario.ASlibScenario()
        self.scenario.read_scenario(dn=train_dir)
        if test_dir is not None:
            self.test_scenario = aslib_scenario.ASlibScenario()
            self.test_scenario.read_description(
                fn=os.path.join(test_dir,"description.txt"))
            self.test_scenario.read_feature_values(
                fn=os.path.join(test_dir,"feature_values.arff"))
            self.test_scenario.read_feature_runstatus(
                fn=os.path.join(test_dir,"feature_runstatus.arff"))
        else:
            self.test_scenario = None
        
        # Import feature groups, feature group runstatus and feature values
        self._import_features()
        
        # Import the solvers (algorithms) performances
        self._import_performances()
        
#        if error==-1:
#            self.feature_group_cost = np.zeros((self.numInstance,self.numFeatureGroups))
        
        # Store the timeout for the current scenario
        self.timeout = self.scenario.algorithm_cutoff_time
        
        self.group_ids_train = []
        self.group_ids_test  = []
    
    
    def _count_lines_header(self, filepath):
        '''
        Count the number of line to skip in the input file. All the lines before '@DATA' or '@data'
        (including this very line) are to be skipped.
        
        Parameters :
        ------------
        filepath : string
            path to the input file
        
        Returns :
        ---------
        _number_lines_header : int
            returns the number of line to skip
        '''
        _number_lines_header = 0
        with open(filepath,'r') as fin:
            for line in fin:
                if ("@DATA" in line) or ("@data" in line):
                    _number_lines_header += 1    # data start at the next line
                    break
                _number_lines_header += 1
                
        return _number_lines_header
    
    
    def _import_features(self):
        '''
        '''
        self.feature_group_names = self.scenario.feature_steps
        self.numFeatureGroups    = len(self.feature_group_names)
        self.feature_names       = self.scenario.feature_data.columns.tolist()
        self.instance_list         = np.asarray(self.scenario.feature_data.index.tolist())
        self.removed_instance_list = []
        
        # from aslib_scenario, i assume all pandas.DataFrame have been sorted by the index column
        self.feature_matrix        = self.scenario.feature_data.as_matrix().astype(np.float32)
        self.feature_runstatus     = self.scenario.feature_runstatus_data[self.feature_group_names
                                        ].as_matrix().astype(str)
        self.list_of_runstatus     = np.unique(self.feature_runstatus)
        
        if self.scenario.feature_cost_data is not None:
            self.feature_group_cost  = self.scenario.feature_cost_data[self.feature_group_names
                                            ].as_matrix().astype(np.float32)
        else:
            self.feature_group_cost = np.zeros((self.feature_matrix.shape[0],self.numFeatureGroups))
        
        self.feature_group_compo = {ft_grp:self.scenario.feature_group_dict[ft_grp]['provides']
                                    for ft_grp in self.scenario.feature_steps}
        self.feature_group_precedence = {ft_grp:self.scenario.feature_group_dict[ft_grp]['requires']
                                         if 'requires' in self.scenario.feature_group_dict[ft_grp]
                                         else []
                                         for ft_grp in self.scenario.feature_steps}
        # dict whose keys are the feature groups names and values are the matching columns of the
        # feature values matrix:
        self.feature_groups = {ft_grp:[ift for ft_in_grp in allft_in_grp 
                                       for ift,ft in enumerate(self.feature_names) if ft==ft_in_grp]
                               for ft_grp,allft_in_grp in self.feature_group_compo.items()}
    
    
    def _import_performances(self):
        '''
        '''
        self.algorithm_list = np.asarray(self.scenario.performance_data.columns.tolist())
        self.numAlg = len(self.algorithm_list)
        
        # from aslib_scenario, i assume all pandas.DataFrame have been sorted by the index column
        self.original_performance_matrix = self.scenario.performance_data[self.algorithm_list
                                                ].as_matrix().astype(np.float32)
        self.runstatus_matrix            = self.scenario.runstatus_data[self.algorithm_list
                                                ].as_matrix().astype(str)
        self.runstatusOK_matrix          = self.runstatus_matrix=='ok'
        self.numInstance, self.numAlg = self.original_performance_matrix.shape
    
    
    def _import_cross_validation(self, cross_validation_file):
        '''
        Import a cross-validation specified in an ARFF file
        
        Parameters :
        ------------
        cross_validation_file : str
            Path to the cross-validation file
        
        Returns :
        ---------
        True
        '''
        if cross_validation_file is None:
            return False
        
        # Special case where the cross validation is defined as in the ICON challenge evaluation
        if (cross_validation_file is not None
            and os.path.isdir(cross_validation_file) == True
            and self.dataset_name is not None):
            try:
                return self._import_ICON_cross_validation(cross_validation_file, self.dataset_name)
            except:
                print('Tentative failed to import cross-validation from a directory. Random splits'+
                      ' will be used instead.')
                return False
        
        with open(cross_validation_file,'r') as fcv:
            data = arff.load(fcv)
        self.xval = 0
        self.group_ids_test = []
        
        for inst in data['data']:
            # if the cross-validation fold is greater than the size of the group_ids (beware, the 
            # fold starts at 1 in the cv.arff files)
            while (int(inst[-1]) > self.xval):
                self.group_ids_test.append([])
                self.xval += 1
            
            # Find the instance whose name is the current. Do something only if exactly one instance
            # id has been found.
            i_inst, = np.where(self.instance_list == inst[0])
            if len(i_inst) == 1:
                self.group_ids_test[int(inst[-1])-1].append(i_inst.tolist()[0])
        
        all_ids = np.repeat(np.arange(self.numInstance).reshape((1,-1)), self.xval, axis=0)
        self.group_ids_train = [np.delete(group_train, group_test, axis=0)
                                for group_train,group_test in zip(all_ids, self.group_ids_test)]
        
        return True
    
    
    def _import_ICON_cross_validation(self, cross_validation_dir, dataset_name):
        '''
        '''
        
        group_ids_test = []
        for i_xval in range(self.xval):
            test_set_definition = os.path.join(cross_validation_dir,
                                               '{0}-{1}-test-truth'.format(dataset_name,i_xval),
                                               'algorithm_runs.arff')
            
#            print('{} being read.'.format(test_set_definition))
            
            data = None
            try:
                with open(test_set_definition,'r') as fin:
                    data  = arff.load(fin)
            except:
                print(sys.exc_info()[0])
                return False
            
            if data is None:
                return False
            
            xval_ids_test = []
            
            for inst in data['data']:
                # Find the instance whose name is the current. Do something only if exactly one 
                # instance id has been found.
                i_inst, = np.where(self.instance_list == inst[0])
                if len(i_inst) == 1:
                    xval_ids_test.append(i_inst.tolist()[0])
            
            group_ids_test.append(list(set(xval_ids_test)))
        
        self.group_ids_test = group_ids_test
        
        group_ids_train = []
        for i_xval in range(self.xval):
            train_set_definition = os.path.join(cross_validation_dir,
                                               '{0}-{1}-train'.format(dataset_name,i_xval),
                                               'algorithm_runs.arff')
            
            #print('{} being read.'.format(train_set_definition))
            
            data = None
            try:
                with open(train_set_definition,'r') as fin:
                    data  = arff.load(fin)
            except:
                print(sys.exc_info()[0])
                return False
            
            if data is None:
                return False
            
            xval_ids_train = []
            
            for inst in data['data']:
                # Find the instance whose name is the current. Do something only if exactly one 
                # instance id has been found.
                i_inst, = np.where(self.instance_list == inst[0])
                if len(i_inst) == 1:
                    xval_ids_train.append(i_inst.tolist()[0])
            
            group_ids_train.append(list(set(xval_ids_train)))
        
        self.group_ids_train = group_ids_train
        
        self.use_challenge_split = True
        
        return True
    
    
    def dump_algorithm_selection_arff(self, filename, instanceID, runID, suggested_algorithm, time_limit):
        '''
        Writes a scheduling down to a file with the format specified for the AS ICON challenge. Each
        step of the scheduling consists in one step on one instance and is characterized by a 
        suggested algorithm and a time to run it. Different steps on the same instance should be put
        in a row.
        
        Parameters :
        ------------
        filename : str
            Name of the output file. WARNING: if it already exists, it will be replaced.
        instanceID : array (n_steps,1) dtype=str
            Instance names of each step of the global schedule.Different steps on the same instance
            should be put in a row.
        runID : array (n_steps,1) dtype=int
            Order of the solver to be run on the instance (lowest is first, start at 1). For a given
            instance name, the runID should be all differents.
        suggested_algorithm : array (n_steps,1) dtype=str
            Name of the solver to be run at each step. Each solver can be run at most once on each 
            instance.
        time_limit : array (n_steps,1) dtype=float
            Time to run the solver at each step. For ASlib, the sum of the time_limit on the same
            instance should not exceed the time cutoff of the scenario.
        '''
        dataset = os.path.basename(os.path.dirname(self.feature_values_filepath))
        
        data = {'relation':'AS',
                'description':dataset,
                'attributes':[('instanceID','STRING'),
                              ('runID','INTEGER'),
                              ('solver','STRING'),
                              ('time_limit','NUMERIC')
                              ],
                'data':np.hstack([instanceID,runID,suggested_algorithm,time_limit]).tolist()
                }
        with open(filename,'w') as fout:
            arff.dump(data,fout)    # WARNING: I changed the arff code a bit so that it works!!!
    
    
    def dump_algorithm_selection(self, filename, instanceID, runID, suggested_algorithm, time_limit):
        '''
        Writes a scheduling down to a file with the format specified for the AS ICON challenge. Each
        step of the scheduling consists in one step on one instance and is characterized by a 
        suggested algorithm and a time to run it. Different steps on the same instance should be put
        in a row.
        
        Parameters :
        ------------
        filename : str
            Name of the output file. WARNING: if it already exists, it will be replaced.
        instanceID : array (n_steps,1) dtype=str
            Instance names of each step of the global schedule.Different steps on the same instance
            should be put in a row.
        runID : array (n_steps,1) dtype=int
            Order of the solver to be run on the instance (lowest is first, start at 1). For a given
            instance name, the runID should be all differents.
        suggested_algorithm : array (n_steps,1) dtype=str
            Name of the solver to be run at each step. Each solver can be run at most once on each 
            instance.
        time_limit : array (n_steps,1) dtype=float
            Time to run the solver at each step. For ASlib, the sum of the time_limit on the same
            instance should not exceed the time cutoff of the scenario.
        '''
        instanceID = instanceID.ravel()
        runID = runID.ravel()
        suggested_algorithm = suggested_algorithm.ravel()
        time_limit = time_limit.ravel()
        str_out = ''
        for (inst,runid,solver,runtime) in zip(instanceID, runID, suggested_algorithm, time_limit):
            str_out += '{0},{1},{2},{3:.2f}\n'.format(inst,runid,solver,runtime)
        with open(filename,'w') as fout:
            fout.write(str_out)
    
    
    def dump_schedule_oasc(self, schedule, name_output_file):
        '''
        '''
        import json
        
        # dump results to disk
        # overwrites old results!
        with open(name_output_file, "w") as fp:
            json.dump(schedule, fp=fp, indent=2)
    
    
    def _drop_unsolvable_instances(self, inst, feat, perfOut, perfLearning, ranking, runstatusOK,
                                   featRunstatus):
        '''
        '''
        n_inst = len(feat)
        solvable_instances               = inst.copy()
        feat_solvable_instances          = feat.copy()
        perfOut_solvable_instances       = perfOut.copy()
        perfLearning_solvable_instances  = perfLearning.copy()
        ranking_solvable_instances       = ranking.copy()
        runstatusOK_solvable_instances   = runstatusOK.copy()
        if featRunstatus is not None:
            featRunstatus_solvable_instances = featRunstatus.copy()
            i_presolved_inst = np.where(featRunstatus=='presolved')[0]
        else:
            featRunstatus_solvable_instances = None
            i_presolved_inst = []
        
        for i_inst in range(n_inst-1,-1,-1):
            if (np.sum(runstatusOK[i_inst,:]) == 0) and (i_inst not in i_presolved_inst):
                solvable_instances               = np.delete(solvable_instances, i_inst, axis=0)
                feat_solvable_instances          = np.delete(feat_solvable_instances, i_inst,
                                                             axis=0)
                perfOut_solvable_instances       = np.delete(perfOut_solvable_instances, i_inst,
                                                             axis=0)
                perfLearning_solvable_instances  = np.delete(perfLearning_solvable_instances, i_inst,
                                                             axis=0)
                ranking_solvable_instances       = np.delete(ranking_solvable_instances, i_inst,
                                                             axis=0)
                runstatusOK_solvable_instances   = np.delete(runstatusOK_solvable_instances, i_inst,
                                                             axis=0)
                if featRunstatus is not None:
                    featRunstatus_solvable_instances = np.delete(featRunstatus_solvable_instances,
                                                                 i_inst, axis=0)
        
        return (solvable_instances,
                feat_solvable_instances,
                perfOut_solvable_instances,
                perfLearning_solvable_instances,
                ranking_solvable_instances,
                runstatusOK_solvable_instances,
                featRunstatus_solvable_instances)
    
    
    def _drop_unsolvable_instances2(self, runstatusOK, featRunstatus, *others):
        '''
        '''
        n_inst = len(runstatusOK)
        if featRunstatus is not None:
            featRunstatus_solvable_instances = featRunstatus.copy()
            i_presolved_inst = np.where(featRunstatus=='presolved')[0]
        else:
            featRunstatus_solvable_instances = None
            i_presolved_inst = []
        
        i_inst_to_keep = []
        for i_inst in range(n_inst):
            if (np.sum(runstatusOK[i_inst,:]) == 0) and (i_inst not in i_presolved_inst):
                pass
            else:
                i_inst_to_keep.append(i_inst)
        
        returns = [runstatusOK[i_inst_to_keep]]
        if featRunstatus is not None:
            returns.append(featRunstatus[i_inst_to_keep])
        else:
            returns.append(None)
        for mat in others:
            returns.append(mat[i_inst_to_keep])
        
        return returns
    
    
    def compute_ICONchallenge_score_from_file(self, fname, count_feature_cost=True):
        '''
        Compute the ICON challenge scores (PAR10 incl. feature costs, number of solved instances,
        performance wrt. the oracle that don't even need features) of a scheduling specified in a
        file
        
        Parameters :
        ------------
        fname : str
            A file that specify a scheduling for each instance of the test run.
        
        Returns :
        ---------
        
        '''
        with open(fname,'r') as fin:
            data = arff.load(fin)
        schedule = data['data']
        instanceID          = np.array(schedule)[:,0].astype(str)
        runID               = np.array(schedule)[:,1].astype(int)
        suggested_algorithm = np.array(schedule)[:,2].astype(str)
        time_limit          = np.array(schedule)[:,3].astype(float)
        
        return self.compute_ICONchallenge_score(instanceID.reshape((-1,1)),
                                                runID.reshape((-1,1)),
                                                suggested_algorithm.reshape((-1,1)),
                                                time_limit.reshape((-1,1)),
                                                count_feature_cost=count_feature_cost
                                                )
    
    
    def compute_ICONchallenge_score(self, instanceID, runID, suggested_algorithm, time_limit,
                                    count_feature_cost=True):
        '''
        Compute the ICON challenge scores (PAR10 incl. feature costs, number of solved instances,
        performance wrt. the oracle that doesn't even need features) of a scheduling specified in
        parameters.
        
        Parameters :
        ------------
        instanceID : array (n_steps,1) dtype=str
            Instance names of each step of the global schedule.Different steps on the same instance
            should be put in a row.
        runID : array (n_steps,1) dtype=int
            Order of the solver to be run on the instance (lowest is first, start at 1). For a given
            instance name, the runID should be all differents.
        suggested_algorithm : array (n_steps,1) dtype=str
            Name of the solver to be run at each step. Each solver can be run at most once on each 
            instance.
        time_limit : array (n_steps,1) dtype=float
            Time to run the solver at each step. For ASlib, the sum of the time_limit on the same
            instance should not exceed the time cutoff of the scenario.
        
        Returns :
        ---------
        a tuple (total PAR10, number of instances solved)
        '''
        assert (instanceID.shape[1]==1)
        assert (instanceID.shape==runID.shape)
        assert (instanceID.shape==suggested_algorithm.shape)
        assert (instanceID.shape==time_limit.shape)
        
        n_instances = len(set(instanceID.flatten().tolist()))
        
        # The presolver step
        # TODO
        presolving_time           = np.zeros((n_instances,), dtype=float)
        is_presolved_per_instance = np.zeros((n_instances,), dtype=bool)
        PAR10                     = np.zeros((n_instances,), dtype=float)
        runtime_presolver = 0.
        presolving_time[is_presolved_per_instance==False] = runtime_presolver
        
        # The feature computing step
        self.feature_groups_used = list(self.feature_group_names) # TODO: select group of features
        # Generate the adequate feature cost matrix
        feat_cost_matrix      = np.zeros((n_instances,self.feature_group_cost.shape[1]),dtype=float)
        feat_runstatus_matrix = np.zeros((n_instances,self.feature_group_cost.shape[1]),dtype='<U68')   ##### changed dtype, check?
        icur = -1       # will be incremented in the first loop
        old_inst = ''   # buffer variable
        # This loops assumes that if there are multiple times the same instanceID, the occurences 
        # are succeeding in a row.
        for inst in instanceID:
            if inst != old_inst:
                icur += 1
                old_inst = inst
                i_inst, = np.where(self.instance_list==inst)
                feat_cost_matrix[icur,:]      = self.feature_group_cost[i_inst,:]
                feat_runstatus_matrix[icur,:] = self.feature_runstatus[i_inst,:]
        
        ft_cost, is_ft_solved = self.compute_feature_cost(feat_cost_matrix,
                                                          feat_runstatus_matrix,
                                                          self.feature_groups_used)
        
        
        # The scheduling step
        (running_time_per_instance,
         is_solved_per_instance) = self.evaluate_schedule(
                                                instanceID, runID, suggested_algorithm, time_limit)
        
        solved = is_presolved_per_instance + is_ft_solved + is_solved_per_instance
        
        PAR10 += presolving_time
        if count_feature_cost==True:
            PAR10 += (is_presolved_per_instance==False) * ft_cost
        PAR10 += (is_presolved_per_instance==False) * (is_ft_solved==False) * running_time_per_instance
        PAR10[solved==False] = 10*self.timeout
        PAR10[PAR10>self.timeout] = 10*self.timeout
        solved[PAR10>self.timeout] = False
        
        return np.sum(PAR10), np.count_nonzero(solved)
    
    
    def compute_feature_cost(self, feat_cost_mat, feat_runstatus_mat, feature_group_used):
        '''
        Given a table of feature group cost per instance and the id of the feature groups used by 
        the system, returns the total cost of computing the feature used for the given instances.
        
        Parameters :
        ------------
        feat_cost_mat: array (n_instances, self.numFeatureGroups)
        
        Returns :
        ---------
        A tuple (cost,solving)
        cost : array (n_instances,) dtype=float
            the cost of computing the required features per instance
        solving : array (n_instances,) dtype=bool
            True if the feature computation has solved the instances, False otherwise
        
        WARNING: is is as if all features were computed in parallel; even if one group of features 
                 solves the instance, the cost of computing the other required features will be paid.
        '''
        n_instances, n_grp = feat_cost_mat.shape
        assert(len(feature_group_used)<=n_grp)
        feat_cost_mat = np.nan_to_num(feat_cost_mat)
        
        cost    = np.zeros((n_instances,), dtype=float)
        solving = np.zeros((n_instances,), dtype=bool)
        self.feature_group_names = np.asarray(self.feature_group_names,dtype=str)
        i_grp_used = np.array([np.where(self.feature_group_names==grp)[0][0] for grp in feature_group_used])
#        print(i_grp_used)
        for grp in feature_group_used:
            i_grp, = np.where(self.feature_group_names==grp)
            if len(i_grp)==1:
                cost += feat_cost_mat[:,i_grp].reshape((-1,))
                
                # case where the group of features solves a few instances
                newly_solved = (feat_runstatus_mat[:,i_grp] == 'presolved').reshape((-1,))
#                print(np.array(feature_group_used))
#                print((feat_runstatus_mat=='presolved').shape)
#                print(feat_runstatus_mat.shape)
#                print(feat_cost_mat[feat_runstatus_mat=='presolved'].shape)
#                np.where((feat_runstatus_mat=='presolved')[:,i_grp_used]
###                feature_solving_cost = np.min(np.where(feat_runstatus_mat[:,i_grp_used]=='presolved',
###                                                       feat_cost_mat[:,i_grp_used],
###                                                       self.timeout),axis=1)
###                
###                cost[newly_solved] = feature_solving_cost[newly_solved]
#                cost[newly_solved] = np.where(feat_cost_mat[:,i_grp][newly_solved]<cost[newly_solved],
#                                              feat_cost_mat[:,i_grp][newly_solved],
#                                              cost[newly_solved])
                solving += newly_solved
            else:
                print('WARNING: the feature groups used are ill-specified.')
                print('self.feature_group_names: {}'.format(self.feature_group_names))
                print('grp: {}'.format(grp))
        
        return cost, solving
    
    
    def evaluate_presolver(self, instanceID, presolver, presolving_time_limit):
        '''
        Simulates the presolving step as a schedule consisting in one single solver.
        
        Parameters :
        ------------
        instanceID : array (n_steps,1) dtype=str
            Instance names of each step of the global schedule.Different steps on the same instance
            should be put in a row.
        presolver : str
            Name of the presolver.
        presolving_time_limit : float
            Time devoted to the presolving phase.
        
        Returns :
        ---------
        A tuple (running_time_per_instance, is_solved_per_instance)
        running_time_per_instance : array (n_instances,) dtype=float
            Runtime when applying the schedule to the instance
        is_solved_per_instance : array (n_instances,) dtype=bool
            True if the instance has been solved by the schedule, False otherwise
        '''
        _, unique_instanceID_idx = np.unique(instanceID, return_index=True)
        unique_instanceID = instanceID[np.sort(unique_instanceID_idx)].reshape((-1,1))
        return self.evaluate_schedule(unique_instanceID,
                                      np.ones(unique_instanceID.shape,dtype=int),
                                      np.repeat(np.array([[presolver]],dtype=str),
                                                unique_instanceID.shape[0],
                                                axis=0),
                                      np.repeat(np.array([[presolving_time_limit]],dtype=float),
                                                unique_instanceID.shape[0],
                                                axis=0))
    
    
    def evaluate_schedule(self, instanceID, runID, suggested_algorithm, time_limit):
        '''
        Simulates a scheduling and return for each specified instance the simulated running time
        (beware: NOT the PAR10) and if the instance has been solved.
        
        Parameters :
        ------------
        filename : str
            Name of the output file. WARNING: if it already exists, it will be replaced.
        instanceID : array (n_steps,1) dtype=str
            Instance names of each step of the global schedule.Different steps on the same instance
            should be put in a row.
        runID : array (n_steps,1) dtype=int
            Order of the solver to be run on the instance (lowest is first, start at 1). For a given
            instance name, the runID should be all differents.
        suggested_algorithm : array (n_steps,1) dtype=str
            Name of the solver to be run at each step. Each solver can be run at most once on each 
            instance.
        time_limit : array (n_steps,1) dtype=float
            Time to run the solver at each step. For ASlib, the sum of the time_limit on the same
            instance should not exceed the time cutoff of the scenario.
        
        Returns :
        ---------
        A tuple (running_time_per_instance, is_solved_per_instance)
        running_time_per_instance : array (n_instances,) dtype=float
            Runtime when applying the schedule to the instance
        is_solved_per_instance : array (n_instances,) dtype=bool
            True if the instance has been solved by the schedule, False otherwise
        '''
        assert (instanceID.shape[1]==1), 'instanceID.shape: {}'.format(instanceID.shape)
        assert (instanceID.shape==runID.shape), 'runID.shape: {}'.format(runID.shape)
        assert (instanceID.shape==suggested_algorithm.shape), 'suggested_algorithm.shape: {}'.format(suggested_algorithm.shape)
        assert (instanceID.shape==time_limit.shape), 'time_limit.shape: {}'.format(time_limit.shape)
        
        # Dictionary to link an algorithm name to its index in self.algorithm_list
        i_algo = {ID:i_a for i_a,ID in enumerate(self.algorithm_list)}
        
        global_schedule = pd.DataFrame(np.hstack([instanceID,runID,suggested_algorithm,time_limit]),
                                       columns=['inst','runid','algo','time']
                                       )
        global_schedule['runid'] = global_schedule['runid'].astype(int)
        global_schedule['time'] = global_schedule['time'].astype(float)
        instanceID_noduplicates = global_schedule['inst'].drop_duplicates().as_matrix().astype(str)
        n_instance = len(instanceID_noduplicates)
#        _, unique_instanceID_idx = np.unique(instanceID, return_index=True)
#        unique_instanceID = instanceID[np.sort(unique_instanceID_idx)].reshape((-1,))
        unique_instanceID, unique_instanceID_idx = np.unique(instanceID, return_index=True)
        running_time_per_instance = np.zeros((n_instance,), dtype=float)
        is_solved_per_instance    = np.zeros((n_instance,), dtype=bool)
        
        for cur_ind,inst in enumerate(unique_instanceID):
            # Select all the lines whose 'inst' field is the current instance, sort them by 
            # 'runid' and turns the result into a numpy array.
#            print(inst)
#            print(global_schedule['inst'])
            instance_schedule = global_schedule[global_schedule['inst']==inst].sort_values(
                                by='runid',inplace=False).as_matrix()
            
            # Load the instance performances
            i_inst, = np.where(self.instance_list == inst)
            if len(i_inst) != 1:
                print('WARNING: instanceID "{}" could not be found in the data'.format(inst))
                continue
            perf = self.original_performance_matrix[i_inst,:].reshape((-1,))
            rsOK = self.runstatusOK_matrix[i_inst,:].reshape((-1,))
            
            step = 0
            for isch,sch in enumerate(instance_schedule):
                _,runid,algo,time = sch
                runid = int(runid)
                time  = float(time)
                # As the schedule has been sorted by increasing runid, the following assertion is 
                # sufficient to detect if the runid are all differents.
                assert (step<runid), 'All runID must be differents and strictly positive integers: {}<{} for {}'.format(step,runid,sch[0])
                step = runid
                
                if ((is_solved_per_instance[cur_ind] == False) and
                    (running_time_per_instance[cur_ind] < self.timeout)
                   ):
                    runtime_algo = perf[i_algo[algo]]
                    if runtime_algo <= time:
#                        running_time_per_instance[cur_ind] += runtime_algo
                        if rsOK[i_algo[algo]] == True:     # 2015/11/25: added this condition
                            running_time_per_instance[cur_ind] += runtime_algo
                            is_solved_per_instance[cur_ind]    = True
                        else:
                            running_time_per_instance[cur_ind] += runtime_algo
                    else:   # the selected algo cannot solve the instance during this step
                        running_time_per_instance[cur_ind] += time
                    
                    # Check if the timeout has not been exceeded
                    if running_time_per_instance[cur_ind] > self.timeout:
                        running_time_per_instance[cur_ind] = self.timeout
                        is_solved_per_instance[cur_ind]    = False
        
        
        return running_time_per_instance, is_solved_per_instance
    




