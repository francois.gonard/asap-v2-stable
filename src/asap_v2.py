################################################################################
# Réduction de modèles et Optimisation Multi-physiques
# Copyright (c) 2014, 2015, 2017 Institut de Recherche Technologique SystemX
# All rights reserved.
################################################################################
#    
#    This file is part of ASAP.V2.
#    
#    ASAP.V2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    ASAP.V2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with ASAP.V2.  If not, see <http://www.gnu.org/licenses/>.
#    

import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as ss
import scipy.linalg
import pickle
import sklearn
import cma

import asap_v1
import MatLearning

__author__ = "Francois Gonard"
__email__ = "francois.gonard@irt-systemx.fr"
__version__ = "2.0"
__license__ = "GPL version 3"


class ASAP_V2(asap_v1.ASAP_V1):
    """
    Algorithm selector and scheduler for the ICON challenge.
    """
    
    def __init__(self,
                 train_dir,
                 test_dir=None,
                 learner_ = 'SVR',
                 learner_opts_ = [],
                 performance_measure_ = 'PAR10',
                 performance_learnt_ = 'PAR10',
                 print_to_file_ = False,
                 name_file_out_ = 'file.out',
                 size_preschedule_ = 3,
                 max_runtime_preschedule_ = -1,
                 random_seed = -1,
                 verbosity=0,
                 **kwargs
                 ):
        '''
        Initialize the object and imports the data.
        '''
        super(ASAP_V2,self).__init__(train_dir,
                                     test_dir = test_dir,
                                     learner_ = learner_,
                                     learner_opts_ = learner_opts_,
                                     performance_measure_ = performance_measure_,
                                     performance_learnt_ = performance_learnt_,
                                     print_to_file_ = print_to_file_,
                                     name_file_out_ = name_file_out_,
                                     size_preschedule_ = size_preschedule_,
                                     max_runtime_preschedule_ = max_runtime_preschedule_,
                                     random_seed = random_seed,
                                     verbosity=verbosity,
                                     kwargs=kwargs
                                     )
        
        self.regularization_weight = 0.
        self.variance_weight = 0.
    
    
    def _optimize_preschedule_cma_fixed_algorithm(self,
                                                  feature_train,
                                                  performanceForLearning_train,
                                                  performanceOut_train,
                                                  ialgos_preschedule_init,
                                                  runtimes_preschedule_init,
                                                  regularization_weight_factor=0.0,
                                                  variance_weight_factor=0.0,
                                                  feature_test=None,
                                                  performanceOut_test=None):
        '''
        Optimization of runtimes of the preschedule using CMA-ES
        '''
        size_train = feature_train.shape[0]
        
        if feature_test is None:
            # !!! Prediction on training set !!!
            feature_test        = feature_train
            performanceOut_test = performanceOut_train
            size_test           = feature_test.shape[0]
        
        ### Simulate another algo which is actually the selector ###
        if hasattr(self, 'optimize_against_oracle') and self.optimize_against_oracle == True:
            # The best algorithm will be selected among the true performances
            perf_pred = performanceOut_train.copy()
        else:
            # Predict performances to select the best predicted solver from
            perf_pred = np.zeros((size_test,self.numAlg))
            for i,inst in enumerate(feature_test):
                perf_pred[i] = self.PerfModel.predict(inst)
        best_algorithm_pred_test = self._selectKBestAlgorithmsFromPerformance(perf_pred, k=1).reshape((-1,))
        
        # Build the "selector algorithm" performances
        if hasattr(self, 'optimize_against_noas') and self.optimize_against_noas == True:
            # The selector is not considered. It is simulated with an algorithm that fails every
            # time
            selector_column_test = 10*self.timeout * np.ones((performanceOut_test.shape[0],1))
        else:
            selector_column_test = np.array([performanceOut_test[i_inst,best_algorithm_pred_test[i_inst]]
                                              for i_inst in range(size_test)]
                                            ).reshape((-1,1))
        
        extd_performance_matrix_test = np.hstack([performanceOut_test, selector_column_test])
        
        algorithm_description = np.eye(self.numAlg+1)
        total_runtime_preschedule_xval = np.sum(runtimes_preschedule_init)
        
        # Append the "selector algorithm" to the schedule
        ialgos_preschedule_   = np.append(ialgos_preschedule_init, self.numAlg)
        runtimes_preschedule_ = np.append(runtimes_preschedule_init,
                                          np.maximum(self.timeout-np.sum(runtimes_preschedule_init),0.))
        
        
        def encode_runtimes(rt):
            '''
            Encode runtime rt
            '''
            rt_ = rt/total_runtime_preschedule_xval
            x_ = np.zeros((rt_.size-2,))
            if np.sum(rt_[:-1])<=1.:
                x_ = rt_[:-2]
            else:
                for irt in range(len(rt_)-2):
                    if np.cumsum(rt_)[irt]<1.:
                        x_[irt] = rt_[irt]
                    else:
                        x_[irt] = 1.-np.sum(x_[:irt])
            return x_
        
        def decode_runtimes(x):
            '''
            Decode a runtime coded as x in [0,1]
            '''
            x_ = np.abs(x)
            rt = np.zeros((x_.size+2,))
            if np.cumsum(x_)[-1]<=1.:
                rt[:-2] = x_*total_runtime_preschedule_xval
            else:
                rt[:-2] = np.where(1.-np.cumsum(x_)>=0,
                                    x_*total_runtime_preschedule_xval,
                                    np.maximum(np.roll(1.-np.cumsum(x_),1),np.zeros(x_.shape))
                                                                *total_runtime_preschedule_xval
                                    )
                if x_[0]>1.:
                    rt[0] = total_runtime_preschedule_xval
            
            rt[-2] = np.maximum((1.-np.cumsum(x_)[-1])*total_runtime_preschedule_xval,0)
            rt[-1] = self.timeout-np.sum(rt[:-1])
            
            return rt
        
        
        def fobj_fixed_algorithm(x_):
            x = np.abs(x_).reshape((-1,))
            
            perf_mat = extd_performance_matrix_test
            if regularization_weight_factor<-1.e-6:
                # Choose xx% of the instances to evaluate on (by default with replacement)
                perf_mat = extd_performance_matrix_test[
                    np.random.choice(np.arange(len(extd_performance_matrix_test)),
                    size=int(-regularization_weight_factor*len(extd_performance_matrix_test)))
                    ]
            
            if variance_weight_factor<-1.e-6:
                fitness = self._get_time_to_solve_triangular(perf_mat,
                                                  algorithm_description,
                                                  ialgos_preschedule_,
                                                  x,
                                                  -variance_weight_factor
                                                  )
            else:
                fitness = self._get_time_to_solve(perf_mat,
                                                  algorithm_description,
                                                  ialgos_preschedule_,
                                                  x
                                                  )
                # or use self._get_time_to_solve_simple(...)
            
            partial_variance = np.var(fitness[fitness<self.timeout])
            
            return np.sum(fitness),partial_variance
        
        
        def fobj_fixed_algorithm_regularized_1(x_):
            x = decode_runtimes(np.abs(x_))
            rts_p = x[:-1]
            
            runtime_res, partial_var_res = fobj_fixed_algorithm(x)
            reg = np.linalg.norm(rts_p/np.sum(rts_p)) # ~total time allocated to the preschedule
            cto = 0.
            if regularization_weight_factor>0.:
                cto = regularization_weight_factor * len(extd_performance_matrix_test)*self.timeout
            var_pen = 0.
            if variance_weight_factor>0.:
                var_pen = partial_var_res
            
            return runtime_res + cto*reg + variance_weight_factor*var_pen
        
        
        # Encode initial guess
        schedule_ini = encode_runtimes(runtimes_preschedule_.astype(float)).reshape((-1,))
        
        if len(schedule_ini)>=2:    # cmaes
            import cma
            
            opts = cma.CMAOptions()
            opts.set('verbose',-1)
            opts.set('verb_disp',0)
            seed_ = self.rand_st.randint(10000)
            opts.set('seed',seed_)
            
            schedule_ini_str = 'np.array(['+','.join([str(step) for step in schedule_ini])+'])'
    #        res = cma.fmin(fobj_fixed_algorithm,schedule_ini_str,0.25,options=opts,restarts=3,bipop=True);
            
            max_try = 10
            for ii in range(max_try):
                try:
                    res = cma.fmin(fobj_fixed_algorithm_regularized_1,schedule_ini_str,
                                   0.25,options=opts,restarts=3,bipop=True);
                    break
                except (AssertionError, ValueError):
                    print('Failed {} times'.format(ii+1))
                    seed_ = self.rand_st.randint(10000)
                    opts.set('seed',seed_)
                    print('New seed: {}'.format(seed_))
        else:
            from scipy.optimize import minimize_scalar
            res_optim = minimize_scalar(lambda x:fobj_fixed_algorithm_regularized_1(np.asarray([x])),
                                        bounds=(0,1),
                                        method='bounded')
            res = np.asarray([res_optim.x])
        
        # do not forget to decode result
        return (decode_runtimes(res[0]),
                algorithm_description[ialgos_preschedule_],
                algorithm_description
                )
    
    
    def _get_time_to_solve_triangular(self, performanceOut, algorithm_description, 
                                      step_solver_description, runtimes, semi_width,
                                      time_limit=-1):
        
        if time_limit<0:
            time_limit = self.timeout
        
        if step_solver_description.ndim > 1:
            if np.max(step_solver_description) > np.max(algorithm_description)+1:
                return performanceOut.shape[0]*10*timeout
            if np.min(step_solver_description) < np.min(algorithm_description)-1:
                return performanceOut.shape[0]*10*timeout
        
        # Identify the algorithms
        if step_solver_description.ndim == 1:
            i_solvers = step_solver_description
        else:
            num_algo,num_algo_feat = algorithm_description.shape
            i_solvers = np.zeros((n_step,),dtype=int)
            for i_s,s in enumerate(step_solver_description):
                i_solvers[i_s] = np.argmin(np.linalg.norm(algorithm_description-s,axis=1))
        
        time_to_solve = self.compute_runtime_triangular(performanceOut[:,i_solvers],runtimes,semi_width,
                                                        time_limit=time_limit)
        
        time_to_solve[time_to_solve>time_limit] = 10.*time_limit
        
        return time_to_solve
    
    
    def compute_runtime_triangular(self,V,W,D,time_limit=10.,printout=False):
        '''
        '''
        
        # Computation functions for the triangular-distributed instances
        k = 1.
        def int1(v,w,D):
            return (3*k-2.)*v*np.ones(w.shape)
        def pp1(v,w,D):
            #assert v.shape[1]==w.size
            return np.ones((v.shape))
        
        def int2(v,w,D):
            w = w.reshape((1,-1))
            return -k/D**2*((v-D)*(v**2-(v-D)**2)/2 + ((v-D)**3-v**3)/3 + 
                         (v+D)*(v**2-w**2)/2 + (w**3-v**3)/3 + w*(v+D-w)*(v+D+w)/2 - w*(v+D)*(v+D-w))
        def pp2(v,w,D):
            w = w.reshape((1,-1))
            return 0.5 + 1./D**2*(-(w-v)**2/2+D*(w-v))
        
        def int3(v,w,D):
            w = w.reshape((1,-1))
            return k/D**2*((D-v)*(w**2-(v-D)**2)/2 + (w**3-(v-D)**3)/3 + w*(D-v)*(v-w) + w*(v**2-w**2)/2) + k*w/2
        def pp3(v,w,D):
            w = w.reshape((1,-1))
            return 1./D**2*(w+D-v)**2/2
            
        def int4(v,w,D):
            w = w.reshape((1,-1))
            return k*w*np.ones(v.shape)
        def pp4(v,w,D):
            return np.zeros((v.shape))
        
        def f_propor_time(V,W,D):
            '''
            Returns the proportion of instances solved by each step defined by runtimes W and the
            mean time that it took.
            D represent half the width of the triangle.
            '''
            if D < 1.e-10:
                return (np.where(V<W-D, pp1(V,W,D),
                                        pp4(V,W,D)
                                 ),
                        np.where(V<W-D, int1(V,W,D),
                                        int4(V,W,D)
                                 ))
            
            return (np.where(V<W-D, pp1(V,W,D),
                    np.where(V<W,   pp2(V,W,D),
                    np.where(V<W+D, pp3(V,W,D),
                                    pp4(V,W,D)
                             ))),
                    np.where(V<W-D, int1(V,W,D),
                    np.where(V<W,   int2(V,W,D),
                    np.where(V<W+D, int3(V,W,D),
                                    int4(V,W,D)
                             ))))
        
        
        (propor_solved, cost_step) = f_propor_time(V,W,D)
        
        if printout:
            print('propor_solved: ',propor_solved)
            print('cost_step: ',cost_step)
        
        cost_step_ext = np.hstack([cost_step,10.*time_limit*np.ones((V.shape[0],1))])
        propor_solved_ext = np.hstack([np.zeros((propor_solved.shape[0],1)),
                                       propor_solved,
                                       np.ones((propor_solved.shape[0],1))
                                       ])
        
        joint_propor_solved = np.cumprod(1.-propor_solved_ext,axis=1)[:,:-1]*propor_solved_ext[:,1:]
        
        joint_propor_solved_et = np.cumsum(joint_propor_solved,axis=1)
        weight_step = (1.-np.hstack([np.zeros((V.shape[0],1)),
                                     np.where(joint_propor_solved_et>=1,1.,joint_propor_solved_et)]))
        
        cost_solved_within_step = np.sum(cost_step_ext.T*joint_propor_solved.T,axis=0)
        cost_unsolved_endofstep = np.dot(weight_step[:,1:],np.hstack([W,np.array([10.*time_limit])]))
        if printout:
            print('cost_solved_within_step: ',cost_solved_within_step)
            print('cost_unsolved_endofstep: ',cost_unsolved_endofstep)
        
        return cost_solved_within_step + cost_unsolved_endofstep
    
    
    def train(self):
        '''
        '''
        # Keep only instances that can be solved
        (self.runstatusOK_train,
         self.feature_runstatus_train,
         self.instanceList_train,
         self.feature_train,
         self.performanceOut_train,
         self.performanceForLearning_train
         ) = self._drop_unsolvable_instances2(self.runstatusOK_train,
                                              self.feature_runstatus_train,
                                              self.instanceList_train,
                                              self.feature_train,
                                              self.performanceOut_train,
                                              self.performanceForLearning_train
                                              )
        
        self.size_preschedule = min(self.size_preschedule,self.numAlg-1)
        if self.size_preschedule>0:
            (self.runtimes_preschedule, 
             self.ialgos_preschedule,
             self.features_preschedule_train) = self._identify_algorithms_for_preschedule(
                                                  self.performanceOut_train,
                                                  size_first_part_schedule=self.size_preschedule,
                                                  max_time_schedule=self.max_runtime_preschedule
                                                  )
            self.size_preschedule = len(self.ialgos_preschedule)
        else:
            self.runtimes_preschedule = np.zeros((0,))
            self.ialgos_preschedule = np.zeros((0,)).astype(int)
            self.features_preschedule_train = None
        
        # Additional features
        self.original_feature_train = self.feature_train.copy()
        self.full_feature_train = self.preprocess_features(self.original_feature_train,
                                                           self.feature_runstatus_train,
                                                           self.features_preschedule_train)
        
        # Feature selection
        self.feature_train = self.train_feature_selection(self.full_feature_train,
                                                          self.performanceForLearning_train,
                                                          self.selection_weight_threshold)
        
        if ('KNeighbors' not in self.learner):
            self.learner_opts['random_state'] = self.rand_st
        
        self.PerfModel = MatLearning.MatLearning(self.feature_train, self.performanceForLearning_train, 
                                                 learner_ = self.learner, 
                                                 learner_opts_=self.learner_opts)
        
        # Preschedule optimization
        if self.size_preschedule>1:
            (runtimes_preschedule,
             desc_algos_preschedule,
             _
             ) = self._optimize_preschedule_cma_fixed_algorithm(
                                              self.feature_train.copy(),
                                              self.performanceForLearning_train.copy(),
                                              self.performanceOut_train.copy(),
                                              self.ialgos_preschedule,
                                              self.runtimes_preschedule,
                                              regularization_weight_factor=self.regularization_weight,
                                              variance_weight_factor=self.variance_weight
                                              )
            
            # only the runtimes have to be updated in this method
            self.runtimes_preschedule = np.delete(runtimes_preschedule,-1)
            
            # Additional features
            # Update preschedule features: is each of ialgos_preschedule able to solve the instance
            # in less than runtimes_preschedule?
            t_max_ps = np.sum(self.runtimes_preschedule)
            self.features_preschedule_train = \
                self.performanceOut_train[:,self.ialgos_preschedule] < self.runtimes_preschedule
            
            self.full_feature_train = self.preprocess_features(self.original_feature_train,
                                                               self.feature_runstatus_train,
                                                               self.features_preschedule_train)
            
            # Build a updated performances models
            if ('KNeighbors' not in self.learner):
                self.learner_opts['random_state'] = self.rand_st
        
            self.PerfModel = MatLearning.MatLearning(self.feature_train, self.performanceForLearning_train, 
                                                     learner_ = self.learner, 
                                                     learner_opts_=self.learner_opts)
            
        else:
            if self.verbosity:
                print("1-D schedule optimization is not implemented.")
        
    
    
    def get_repr_str(self):
        str_  = super(ASAP_V2, self).get_repr_str()
        str_ += 'regularization_weight = {0}'.format(self.regularization_weight) + os.linesep
        str_ += 'variance_weight = {0}'.format(self.variance_weight) + os.linesep
        
        return str_
    

